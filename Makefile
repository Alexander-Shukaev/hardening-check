### Preamble {{{
##  ==========================================================================
##        @file Makefile
##  --------------------------------------------------------------------------
##     @version 0.0.0
##  --------------------------------------------------------------------------
##     @updated 2018-09-26 Wednesday 16:48:03 (+0200)
##  --------------------------------------------------------------------------
##     @created 2018-09-26 Wednesday 02:28:22 (+0200)
##  --------------------------------------------------------------------------
##      @author Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
##  --------------------------------------------------------------------------
##   @copyright Copyright (C) 2018,
##              Alexander Shukaev <http://Alexander.Shukaev.name>.
##              All rights reserved.
##  --------------------------------------------------------------------------
##     @license This program is free software: you can redistribute it and/or
##              modify it under the terms of the GNU General Public License as
##              published by the Free Software Foundation, either version 3 of
##              the License, or (at your option) any later version.
##
##              This program is distributed in the hope that it will be
##              useful, but WITHOUT ANY WARRANTY; without even the implied
##              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
##              PURPOSE.  See the GNU General Public License for more details.
##
##              You should have received a copy of the GNU General Public
##              License along with this program.  If not, see
##              <http://www.gnu.org/licenses/>.
##  ==========================================================================
##  }}} Preamble
##
### Prefix {{{
##  ==========================================================================
### Includes {{{
##  ==========================================================================
ifeq "$(wildcard PREFIX.mk)" ""
$(error PREFIX.mk: No such file or directory)
else
include PREFIX.mk
endif
##  ==========================================================================
##  }}} Includes
##  ==========================================================================
##  }}} Prefix
##
### Header {{{
##  ==========================================================================
### Includes {{{
##  ==========================================================================
include $(PREFIX)header.mk
##  ==========================================================================
##  }}} Includes
##  ==========================================================================
##  }}} Header
##
### Body {{{
##  ==========================================================================
### Includes {{{
##  ==========================================================================
include $(PREFIX)git.mk
include $(PREFIX)make.mk
##  ==========================================================================
##  }}} Includes
##
### Variables {{{
##  ==========================================================================
override HARDENING_CHECK ?= $(CURDIR)/bin/hardening-check
override README_MD       ?= $(PWD)/README.md
##
override HARDENING_CHECK := $(call s/\s/\\s/,$(HARDENING_CHECK))
override README_MD       := $(call s/\s/\\s/,$(README_MD))
##
# Default
override .DEFAULT_GOAL := all
##  ==========================================================================
##  }}} Variables
##
### Targets {{{
##  ==========================================================================
### All {{{
##  ==========================================================================
.PHONY: all
all: git make readme
##  ==========================================================================
##  }}} All
##
### Help {{{
##  ==========================================================================
.PHONY: help
help:
	@$(HARDENING_CHECK) --help
##  ==========================================================================
##  }}} Help
##
### Manual {{{
##  ==========================================================================
.PHONY: man
man:
	@$(HARDENING_CHECK) --man
##
.PHONY: manual
manual: man
##  ==========================================================================
##  }}} Manual
##
### README {{{
##  ==========================================================================
.PHONY: readme
readme: $(README_MD)
##  ==========================================================================
##  }}} README
##
$(README_MD): $(MAKEFILE) $(HARDENING_CHECK)
	@$(if $(VERBOSE),printf "make: Writing file '%s'\n" '$@')
	@echo '```text'           >| '$@'
	@$(HARDENING_CHECK) --man >> '$@'
	@echo '```'               >> '$@'
##  ==========================================================================
##  }}} Targets
##  ==========================================================================
##  }}} Body
##
### Footer {{{
##  ==========================================================================
### Includes {{{
##  ==========================================================================
include $(PREFIX)footer.mk
##  ==========================================================================
##  }}} Includes
##  ==========================================================================
##  }}} Footer
