```text
NAME
    hardening-check - check ELF binaries for security hardening features

SYNOPSIS
    hardening-check [options] [ELF ...]

    Examine a given list of ELF binaries and check for several security
    hardening features, failing if they are not all found.

DESCRIPTION
    This utility checks a given list of ELF binaries for several security
    hardening features that could have been built into them. These features
    are:

    Position Independent Executable (PIE)
      This indicates that a given ELF executable was built in such a way
      that the "text" section of the program can be relocated in memory. To
      take full advantage of this feature, the executing kernel must support
      text Address Space Layout Randomization (ASLR).

    Stack Smashing Protector (SSP)
      This indicates that a given ELF binary was compiled with the gcc(1)
      option -fstack-protector and/or -fstack-protector-strong (e.g. uses
      either __stack_chk_fail or __stack_chk_fail_local). That is the
      program will be resistant to having its stack buffers accidentally
      overflowed and/or deliberately smashed (for example, due to attack).

      CAUTION:

      When a given ELF binary was built without any character arrays
      actually being allocated on stack, this check will lead to false
      alarms (since there is no use of __stack_chk_fail or
      __stack_chk_fail_local), even though it was compiled with the correct
      options.

    Fortified Functions (FFs)
      This indicates that a given ELF binary was compiled with
      -D_FORTIFY_SOURCE=2 and the gcc(1) optimization option -O1 or higher.
      This either substitutes certain vulnerable libc functions with their
      protected counterparts (e.g. strncpy instead of strcpy) or replaces
      calls that are verifiable at run time with the run-time-check version
      (e.g. __memcpy_chk instead of memcpy).

      CAUTION:

      When a given ELF binary was built such that the fortified versions of
      the libc functions are not useful (e.g. use is verified as safe at
      compile time or use cannot be verified at run time), this check will
      lead to false alarms. In an effort to mitigate this misbehavior, the
      check will pass if any fortified function is found, and will fail if
      only unfortified functions are found. Not verifiable conditions also
      pass (e.g. no functions that could have been fortified are found or
      not linked against libc at all).

    String Format Security Functions (SFSFs)
      This indicates that a given ELF binary was compiled with the gcc(1)
      option -Wformat=2.

    Non-Executable Stack (NES)
      This indicates that a given ELF binary was linked with the gcc(1)
      option -Wl,-z,noexecstack (see ld(1)) to have special ELF markings
      (i.e. GNU_STACK) that cause the dynamic linker/loader ld.so(8) to mark
      any stack memory regions as non-executable. This reduces the amount of
      vulnerable memory regions in a program that could be potentially
      exposed to memory corruption attacks.

    Non-Executable Heap (NEH)
      This indicates that a given ELF binary was linked with the gcc(1)
      option -Wl,-z,noexecheap (see ld(1)) to have special ELF markings
      (i.e. GNU_HEAP) that cause the dynamic linker/loader ld.so(8) to mark
      any heap memory regions as non-executable. This reduces the amount of
      vulnerable memory regions in a program that could be potentially
      exposed to memory corruption attacks.

    Relocation Read-Only (RELRO)
      This indicates that a given ELF binary was linked with the gcc(1)
      option -Wl,-z,relro (see ld(1)) to have special ELF markings (i.e.
      RELRO) that cause the dynamic linker/loader ld.so(8) to mark any
      regions of the relocation table as read-only if they were resolved
      before the actual execution begins. This reduces the amount of
      vulnerable memory regions in a program that could be potentially
      exposed to memory corruption attacks.

    Immediate Symbol Binding (NOW)
      This indicates that a given ELF binary was linked with the gcc(1)
      option -Wl,-z,now (see ld(1)) to have special ELF markings (e.g.
      BIND_NOW or NOW) that cause the dynamic linker/loader ld.so(8) to
      resolve all symbols before the actual execution begins. When combined
      with RELRO (see above), this further reduces the amount of vulnerable
      memory regions in a program that could be potentially exposed to
      memory corruption attacks.

OPTIONS
    --no-pie, -e
      Do not require that the checked ELF binaries are built as PIE.

    --no-ssp, -p
      Do not require that the checked ELF binaries are compiled with SSP.

    --no-ffs, -f
      Do not require that the checked ELF binaries are compiled with FFs.

    --no-sfsfs, -s
      Do not require that the checked ELF binaries are compiled with SFSFs.

    --no-nes, -S
      Do not require that the checked ELF binaries are linked with NES.

    --no-neh, -H
      Do not require that the checked ELF binaries are linked with NEH.

    --no-relro, -R
      Do not require that the checked ELF binaries are linked with RELRO.

    --no-now, -N
      Do not require that the checked ELF binaries are linked with NOW.

    --report-ufs, -U
      Additionally, report all undefined (external) functions needed by the
      checked ELF binaries.

    --find-ffs, -F
      Instead of the regular report, find libc for the first given ELF
      binary and report all fortified functions exported by this libc.

    --find-libc, -L
      Instead of the regular report, find libc for the first given ELF
      binary and report the absolute path to this libc.

    --libc
      Explicitly specify the path to libc that is to be searched for
      exported fortified functions at least for checks on object archives
      and object files.

    --color, -c
      Colorize report with ANSI escape sequences.

    --lintian, -l
      Report in Lintian (Debian) format.

    --debug, -d
      Report debug information.

    --quiet, -q
      Report only (not ignored) failures.

    --verbose, -v
      Report verbosely on failures.

    --help, -h, -?
      Print a brief help message and exit.

    --man, -m
      Print a detailed manual page and exit.

RETURN VALUE
    When all the checked ELF binaries support all the specified security
    hardening features, this program will finish with the exit status of 0.
    Otherwise, if any check fails, the exit status will be 1. Each
    individual check can be disabled via a corresponding command line
    option.

AUTHOR
    Kees Cook <kees@debian.org>
    Alexander Shukaev <http://Alexander.Shukaev.name>.

MAINTAINER
    Alexander Shukaev <http://Alexander.Shukaev.name>.

COPYRIGHT
    Copyright (C) 2013, Kees Cook <kees@debian.org>. All rights reserved.
    Copyright (C) 2016, Alexander Shukaev <http://Alexander.Shukaev.name>.
    All rights reserved.

LICENSE
    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the
    Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
    Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program. If not, see <http://www.gnu.org/licenses/>.

SEE ALSO
    hardening-wrapper(1), gcc(1), ld(1), ld.so(8)

REFERENCES
    [ 1] <http://blog.siphos.be/2011/07/high-level-explanation-on-some-binar
    y-executable-security>
    [ 2] <http://en.chys.info/2010/12/note-gnu-stack>
    [ 3] <http://grantcurell.com/2015/09/21/what-is-the-symbol-table-and-wha
    t-is-the-global-offset-table>
    [ 4] <http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.faqs/k
    a14320.html>
    [ 5] <http://lintian.debian.org/tags-all.html>
    [ 6] <http://tk-blog.blogspot.de/2009/02/relro-not-so-well-known-memory.
    html>
    [ 7] <http://wiki.debian.org/Hardening>
    [ 8] <http://wiki.gentoo.org/wiki/Hardened/GNU_stack_quickstart>
    [ 9] <http://wiki.gentoo.org/wiki/Hardened/Toolchain>
    [10] <http://wiki.osdev.org/Stack_Smashing_Protector>
    [11] <http://wiki.ubuntu.com/SecurityTeam/Roadmap/ExecutableStacks>
    [12] <http://wikipedia.org/wiki/Address_space_layout_randomization>
    [13] <http://wikipedia.org/wiki/Buffer_overflow>
    [14] <http://wikipedia.org/wiki/Buffer_overflow_protection>
    [15] <http://wikipedia.org/wiki/Return-oriented_programming>
    [16] <http://wikipedia.org/wiki/Stack_buffer_overflow>
    [17] <http://wikipedia.org/wiki/Uncontrolled_format_string>
    [18] <http://www.owasp.org/index.php/C-Based_Toolchain_Hardening>
    [19] <http://www.win.tue.nl/~aeb/linux/hh/protection.html>

```
